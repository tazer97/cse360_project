import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class SimpleGUI extends JFrame
{

	private JFrame frame;
	
	private JPanel infoPanel, leftPanel, rightPanel, justPanel, spacePanel;;
	
	private JButton chIn, chOut;
	
	public JTextField wordsProcessed,numLines,blankLinesRemoved,avgWordsPerLine,avgLineLength, justType, numOfSpaces, specLineL, spaceCh;

	private JLabel wordsP,lineAmount, lineBlank,avgWPL,lineLength, spaceAmount, specLL;
	
	public JRadioButton justifyL, justifyR, justifyF, singleS, doubleS;
	
	String justifyChooser[] = {"left justified.", "right justified.", "full justified."};
	
	TextFormat tf = new TextFormat();
	
	boolean charNFlag = false;
	
	
	public static void main(String args[])
	{
		new SimpleGUI();
	}
	
	public SimpleGUI()
	{
		frame = new JFrame("Text Formatter");
		frame.setSize(850, 450);
		
		//Panels
		infoPanel = new JPanel();
		leftPanel = new JPanel();
		rightPanel = new JPanel();
		justPanel = new JPanel();
		spacePanel = new JPanel();

		infoPanel.setLayout(new GridLayout(1,2));
		rightPanel.setLayout(new GridLayout(10,1));
		leftPanel.setLayout(new GridLayout(10,1));
		justPanel.setLayout(new FlowLayout());
		
		//analysis text fields and labels
		wordsProcessed = new JTextField();
		wordsProcessed.setEditable(false);
		wordsP = new JLabel("Words Processed: ");
		
		numLines = new JTextField();
		numLines.setEditable(false);
		lineAmount = new JLabel("Number of Lines: ");
		
		blankLinesRemoved = new JTextField();
		blankLinesRemoved.setEditable(false);
		lineBlank = new JLabel("Number of Blank Lines Removed: ");

		avgWordsPerLine = new JTextField();
		avgWordsPerLine.setEditable(false);
		avgWPL = new JLabel("Average Words Per Line: ");
		
		avgLineLength = new JTextField();
		avgLineLength.setEditable(false);
		lineLength = new JLabel("Average Line Length: ");
		
		numOfSpaces = new JTextField();
		numOfSpaces.setEditable(false);
		spaceAmount = new JLabel("Number of spaces in the file: ");
		
		specLineL = new JTextField();
		specLineL.setEditable(true);
		specLineL.setText("80");//default line length
		specLL = new JLabel("Please enter a line length between 20 and 100: ");
		
		justType = new JTextField();
		justType.setEditable(false);
		
		spaceCh = new JTextField();
		spaceCh.setEditable(false);
		
		//Buttons and Radio Buttons
		chIn = new JButton("Select Input File");
		chIn.addActionListener(new ButtonListener());
		chIn.setPreferredSize(new Dimension(20,20));
		
		chOut = new JButton("Select OutputFile");
		chOut.addActionListener(new ButtonListener());
		chOut.setPreferredSize(new Dimension(20,20));
		chOut.setEnabled(false);
		
		ButtonGroup bg = new ButtonGroup();
		justifyL = new JRadioButton("Left Justify");
		justifyR = new JRadioButton("Right Justify");
		justifyF = new JRadioButton("Fully Justify");
		justifyL.addActionListener(new ButtonListener());
		justifyR.addActionListener(new ButtonListener());
		justifyF.addActionListener(new ButtonListener());
		bg.add(justifyL);
		bg.add(justifyR);
		bg.add(justifyF);	
		justifyL.setSelected(true);//default is left
		justPanel.add(justifyL);
		justPanel.add(justifyR);
		justPanel.add(justifyF);
		
		ButtonGroup spaceG = new ButtonGroup();
		singleS = new JRadioButton("Single Spaced");
		doubleS = new JRadioButton("Double Spaced");
		singleS.addActionListener(new ButtonListener());
		doubleS.addActionListener(new ButtonListener());
		spaceG.add(singleS);
		spaceG.add(doubleS);
		
		spacePanel.add(singleS);
		spacePanel.add(doubleS);
		singleS.setSelected(true);//default is single spaced
		spaceCh.setText("The output will be single spaced");
		
		//Adding labels and text fields to panels
		leftPanel.add(wordsP);
		rightPanel.add(wordsProcessed);
		leftPanel.add(lineAmount);
		rightPanel.add(numLines);
		leftPanel.add(lineBlank);
		rightPanel.add(blankLinesRemoved);
		leftPanel.add(avgWPL);
		rightPanel.add(avgWordsPerLine);
		leftPanel.add(lineLength);
		rightPanel.add(avgLineLength);
		leftPanel.add(spaceAmount);
		rightPanel.add(numOfSpaces);
		leftPanel.add(specLL);
		rightPanel.add(specLineL);
		leftPanel.add(justPanel);
		rightPanel.add(justType);
		leftPanel.add(spacePanel);
		rightPanel.add(spaceCh);
		leftPanel.add(chIn);
		rightPanel.add(chOut);
		
		//Add left and right panel to whole infoPanel
		infoPanel.add(leftPanel);
		infoPanel.add(rightPanel);
		
		frame.add(infoPanel);//add Panel to frame
		frame.setVisible(true);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		justType.setText("The output will be " + justifyChooser[tf.jc]);//shows left as default at beginning
	}
	
	private class ButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			//Show how the text will be justified based on radio buttons
			if(event.getSource() == justifyL || event.getSource() == justifyR || event.getSource() == justifyF)
			{
				try 
				{
					if (justifyL.isSelected() == true) 
					{
						tf.jc = 0;	//left justify (call jc from TextFormat)
					}
					if (justifyR.isSelected() == true) //else justifyR is selected
					{
						tf.jc = 1;	//right justify (call jc from TextFormat)
					}
					if (justifyF.isSelected() == true)
					{
						tf.jc = 2;	//full justify (call jc from TextFormat)
					}
					justType.setText("The output will be " + justifyChooser[tf.jc]);
				}
				
				catch (Exception e) 
				{
					System.out.println("Some error with radio button");
				}
			}//end justify listening
			
			if(event.getSource() == singleS || event.getSource() == doubleS) 
			{
				try 
				{
					if (singleS.isSelected() == true) 
					{
						tf.spacing = 1;// single spaced
						spaceCh.setText("The output will be single spaced");
					}
					
					if (doubleS.isSelected() == true) 
					{
						tf.spacing = 2;//double spaced
						spaceCh.setText("The output will be double spaced");
					}
					
				}
				
				catch (Exception e) {
					System.out.println("Some error with radio button");
				}
			}
			
			//Choose input - reset analysis and choose another file
			if(event.getSource() == chIn)
			{
				try
				{	//Show blanks in analysis text fields
					wordsProcessed.setText("");
					numLines.setText("");
					avgWordsPerLine.setText("");
					blankLinesRemoved.setText("");
					avgLineLength.setText("");
					numOfSpaces.setText("");
					
					//this code resets the analysis and file scanner variables in TextFormat
					tf.wordsP = 0;
					tf.lines = 1;
					tf.blanklines = 0;
					tf.avgwords = 0;
					tf.avglength = 0;
					tf.charInLine = 0;
					tf.allChars = 0;
					tf.spaces = 0;
					
					tf.pickInputFile();
					if (tf.inputFileChosen == true) {
						chOut.setEnabled(true);//enable choose output file button
					}
				}
				catch (Exception e)
				{
					chOut.setEnabled(false);
					System.out.println("File was not chosen"); // have to pop up a window
				}
			}//end choose input file
			
			else if(event.getSource() == chOut)
			{
				if(tf.inputFileChosen)
				{
					try
					{
						tf.pickOutputFile();
						
						try
						{
							tf.charPerLine = Integer.parseInt(specLineL.getText());
							charNFlag = true;
						}
						catch(NumberFormatException nfe)  
						{
							charNFlag = false;	
						}
						
						
						if (tf.outputFileChosen) 
						{
							if(tf.charPerLine <= 100 && tf.charPerLine >= 20 && charNFlag && !specLineL.getText().isEmpty() )
							{
								tf.writeToOuput();// actually write the output
								// Show analysis
								wordsProcessed.setText("" + tf.getWordsProcessed());
								numLines.setText("" + tf.getNumLines());
								avgWordsPerLine.setText(tf.getavgWordsPerLine());
								blankLinesRemoved.setText("" + tf.countBlankLinesRemoved());
								avgLineLength.setText(tf.calcAvgLineLength());
								numOfSpaces.setText("" + tf.spaces);
								
								System.out.println("What happened?");
							}
							else
							{
								JOptionPane.showMessageDialog(null,"Please enter a valid line size and pick an Output File.", "Warning", JOptionPane.WARNING_MESSAGE);
							}
							
						}
						else
						{
							JOptionPane.showMessageDialog(null,"Please pick an Output File.", "Warning", JOptionPane.WARNING_MESSAGE);
						}
					}
					catch (Exception e)
					{
						System.out.println("File was not chosen, why?"); // have to pop up a window
					}
				}
				/*
				else // Not needed - kept in case we do
				{	//error message if output button is clicked before input file chosen
					JOptionPane.showMessageDialog(null,"Please pick an Input File first.", "Error", JOptionPane.WARNING_MESSAGE);
				}
				*/
				chOut.setEnabled(false); // resets output file - force to select new input file first
				
			}//end choose output file
			
		}//end actionPerformed
		
	}//end ButtonListener class
	
}//end SimpleGUI class